
from unittest.mock import patch

import main


@patch('builtins.print')
def test__say_hello__no_arguments__prints_hello(mock_print):
    # ARRANGE
    expected = 'Hello'

    # ACT
    main.say_hello()

    # ASSERT
    mock_print.assert_called_once_with(expected)


def test__increment__zero__returns_one():
    # ARRANGE
    expected = 1

    # ACT
    actual = main.increment(0)

    # ASSERT
    assert actual == expected
