# Python Template

## Getting Started
Execute `./.setup_pipenv` to set up the virtual environment.  
Make sure the environment variable `PYTHONPATH` is set to `./python/src`  
Execute `tox` to run unit test and code coverage.
